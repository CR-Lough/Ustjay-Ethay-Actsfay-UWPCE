import os

import requests
import json
from flask import Flask, send_file, Response
from flask.signals import template_rendered
from bs4 import BeautifulSoup


app = Flask(__name__)

template = """
"""

def get_fact():

    response = requests.get("http://unkno.com")

    soup = BeautifulSoup(response.content, "html.parser")
    facts = soup.find_all("div", id="content")

    return facts[0].getText()


@app.route('/')
def home():
    # retrieve a quote from the random simile generator
    response = requests.get("https://random-simile-generator.herokuapp.com/")
    soup = BeautifulSoup(response.content, "html.parser")
    quote = soup.find_all('h1')[1].get_text()

    # send that quote to the pig latinizer
    payload = {'instanceDataJson': template.replace('{}',quote)}
    response = requests.get('https://hidden-journey-62459.herokuapp.com/',params=payload)

    # return the URL to the result
    payload = {'input_text': quote}
    response = requests.post('https://hidden-journey-62459.herokuapp.com/piglatinize/',data=payload)
    
    return response.url


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 6738)) #if we don't try, Heroku chooses for you
    app.run(host='0.0.0.0', port=port)

